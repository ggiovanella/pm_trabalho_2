#Giulio Giovanella; Gabriel Ariel; Carlos Machado.
import cv2
import numpy as np
from matplotlib import pyplot as plt

def app():
    img = cv2.imread('foto.JPG', 1)
    imgBlur = cv2.imread('foto.JPG', 0)
    imgBlur = cv2.medianBlur(imgBlur,5)

    #cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    circuloExterno = cv2.HoughCircles(imgBlur,cv2.HOUGH_GRADIENT,1,20,
                                param1=50,param2=30,minRadius=190,maxRadius=195)
    circuloInterno = cv2.HoughCircles(imgBlur,cv2.HOUGH_GRADIENT,1,20,
                                param1=50,param2=30,minRadius=30,maxRadius=50)

    circuloExterno = np.uint16(np.around(circuloExterno))
    circuloInterno = np.uint16(np.around(circuloInterno))
    circuloExternoFirstColumn = circuloExterno[0,:]
    circuloInternoFirstColumn = circuloInterno[0,:]
    outsideMask = getImgMask(imgBlur, 
            circuloExternoFirstColumn[0][0], 
            circuloExternoFirstColumn[0][1], 
            circuloExternoFirstColumn[0][2],
            False)
    
    img[outsideMask] = 255
    insideMask = getImgMask(imgBlur, 
            circuloInternoFirstColumn[0][0], 
            circuloInternoFirstColumn[0][1], 
            circuloInternoFirstColumn[0][2],
            True)
    paintImg(img, outsideMask)
    paintImg(img, insideMask)
    #img[insideMask] = 255
    #circleMerge(img, circuloExterno)
    #circleMerge(img, circuloInterno)
    display(img)

def paintImg(img, mask):
    for i in range(len(mask)):
        for j in range(len(mask[0])):
            if mask[i][j]:
                for x in range(3):
                    img[i][j][x] = 255
    return

def getImgMask(img, xc, yc, r, inside=True):
    height, width = img.shape
    x, y = np.meshgrid(np.arange(width), np.arange(height))
    # squared distance from the center of the circle
    d2 = (x - xc)**2 + (y - yc)**2
    # mask is True inside of the circle
    if inside:
        return  d2 < r**2
    else:
        return d2 >= r**2

def circleMerge(img, circles):
    for i in circles[0,:]:
        cv2.circle(img,(i[0],i[1]),i[2],(0,255,0),2)
    return img

def display(img):    
    cv2.namedWindow("Img", cv2.WINDOW_NORMAL)
    cv2.imshow('Img', img)
    cv2.waitKey(0)
    return
    plt.imshow(img)
    plt.title('Img')
    plt.show()

app()