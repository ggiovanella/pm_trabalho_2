import cv2 as cv
import numpy as np

img_chuvoso  = cv.cvtColor(cv.imread('Chuvoso.jpg'), cv.COLOR_BGR2GRAY)  
img_estiagem = cv.cvtColor(cv.imread('Estiagem.jpg'), cv.COLOR_BGR2GRAY)

(T, bin1) = cv.threshold(img_chuvoso, 65, 255, cv.THRESH_BINARY_INV)
(T, bin2) = cv.threshold(img_estiagem, 132, 255, cv.THRESH_BINARY_INV) #132


cv.imshow('Chuvoso', img_chuvoso)
cv.imshow('Chuvoso_Tratado', bin1)
cv.imshow('Estiagem', img_estiagem)
cv.imshow('Estiagem_Tratado', bin2)



img_inundacao = bin1 - bin2

img_inundacao = cv.medianBlur(img_inundacao,3)

img_inundacao = cv.cvtColor(img_inundacao, cv.COLOR_GRAY2BGR)

gray = cv.cvtColor(img_inundacao, cv.COLOR_BGR2GRAY)
mask = cv.compare(gray,5,cv.CMP_GT)
img_inundacao[mask > 0] = (0,0,255)

resultado = cv.cvtColor(img_estiagem, cv.COLOR_GRAY2BGR)

for j in range(0, resultado.shape[0]):
    for i in range(0, resultado.shape[1]):
        if 255 == img_inundacao[j, i, 2]:
            resultado[j, i] = (0,0,255)

cv.imshow('Resultado', resultado)

cv.waitKey(0)
